import React from 'react';

export default function Banner() {
    return (
        <>
            <div className="d-flex flex-column align-items-center justify-content-center text-center my-5 mx-lg-5">
                <h2>Bake it 'til you make it!</h2>
                <p className="mt-3 gray"></p>
            </div>
        </>
    );
} 