import { useState, useEffect } from 'react';
import ProductDisplay from './ProductDisplay/ProductDisplay';
import ProductSearch from './ProductSearch';


export default function UserView({productsData}) {

    const [products, setProducts] = useState([])

    useEffect(() => {
        const productsArr = productsData.map(product => {

            //only render the active courses since the route used is /all from Course.js page
            if(product.isActive === true) {
                return (
                    <ProductDisplay productProp={product} key={product._id}/>
                    )
            } else {
                return null;
            }
        })

        //set the courses state to the result of our map function, to bring our returned course component outside of the scope of our useEffect where our return statement below can see.
        setProducts(productsArr)

    }, [productsData])

    return(
        <>
            <ProductSearch/>
            { products }
        </>
        )
}
