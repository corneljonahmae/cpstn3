import React, { useState } from 'react';
import ProductDisplay from './ProductDisplay/ProductDisplay';

const ProductSearch = () => {
  const [searchQuery, setSearchQuery] = useState('');
  const [searchResults, setSearchResults] = useState([]);

  const handleSearch = async () => {
    try {
      const response = await fetch('https://cpstn2-ecommerceapi-dasigan.onrender.com/products/searchByName', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ productName: searchQuery })
      });
      const data = await response.json();
      setSearchResults(data);
    } catch (error) {
      console.error('Error searching for products:', error);
    }
  };

  return (
    <div className='pt-5 container'>
      <h2>Product Search</h2>
      <div className="form-group">
        <label htmlFor="courseName">Product Name:</label>
        <input
          type="text"
          id="courseName"
          className="form-control"
          value={searchQuery}
          onChange={event => setSearchQuery(event.target.value)}
        />
      </div>
      <button className="btn btn-primary" onClick={handleSearch}>
        Search
      </button>
      <h3>Search Results:</h3>
      <ul>
        {searchResults.map(course => (
          //<li key={course.id}>{course.name}</li>
          <CourseCard productProp={product} key={product._id}/>
        ))}
      </ul>
    </div>
  );
};

export default ProductSearch;

