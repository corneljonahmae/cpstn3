import { useContext } from 'react';
import { Navbar, Nav, Container, NavDropdown } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../../UserContext';
import AccountCircleIcon from '@material-ui/icons/AccountCircleRounded';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';


export default function AppNavbar() {

	const { user } = useContext(UserContext);

	return (
		<Navbar sticky='top' collapseOnSelect expand="lg" bg="success" variant="dark">
  			<Container>
  			<Navbar.Brand as={Link} to='/'>Baking Mama</Navbar.Brand>
  			<Navbar.Toggle aria-controls="responsive-navbar-nav" />
  			<Navbar.Collapse id="responsive-navbar-nav">
    			<Nav className="me-auto">
      				<Nav.Link as={NavLink} to='/products/category/Baking Equipment'>Baking Equipment</Nav.Link>
      				<Nav.Link as={NavLink} to='/products/category/Pastry Tools'>Pastry Tools</Nav.Link>
      				<Nav.Link as={NavLink} to='/products/category/Other Baking Stuff'>Other Baking Stuff</Nav.Link>
    			</Nav>
    			<Nav>
      				{
      					(user.id !== null) 
      					?
      					
      					<NavDropdown className="dropdown" title={<div style={{display: "inline-block"}}><AccountCircleIcon/></div>} id="collasible-nav-dropdown">
					        <Nav.Link as={NavLink} to='/dashboard'>Dashboard</Nav.Link>
					       <Nav.Link as={NavLink} to='/logout'>Sign out</Nav.Link>
      					</NavDropdown>
      					:
      					<Nav.Link as={Link} to='/login'> Login</Nav.Link>
      				}
      				<Nav.Link as={Link} to='/cart'>
       					<FontAwesomeIcon icon={faShoppingCart} /> Cart
       				</Nav.Link>
    			</Nav>
  			</Navbar.Collapse>
  			</Container>
		</Navbar>
	)
}