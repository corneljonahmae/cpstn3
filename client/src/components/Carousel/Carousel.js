import Carousel from 'react-bootstrap/Carousel';
import './Carousel.css';
import Button from '../Button/Button';

export default function CarouselSection() {
    return (
        <Carousel variant="light" controls={false} fade>
            <Carousel.Item className="carouselFit" interval={1500}>
                <img
                    className="d-block w-100 carouselImg"
                    src={require('../../assets/images/imgCarousel1.jpg')}
                    alt="First slide"
                />
                <div className="overlay">
                    <h1>Electric Stand Mixer</h1>
                    <h5> It will make mixing doughs and batters faster and easier, and it's by far the best way to mix ingredients into a thick, stiff cookie dough without tiring out your arm.</h5>
                    <Button
                        id='622e6041058c97fbfc045691'
                        buttonStyle='btn--outline'
                        buttonSize='btn--medium'
                    >
                        ORDER NOW
                    </Button>
                </div>
            </Carousel.Item>            
            <Carousel.Item className="carouselFit" interval={1500}>
                <img
                    className="d-block w-100 carouselImg"
                    src={require('../../assets/images/imgCarousel2.jpg')}
                    alt="First slide"
                />
                <div className="overlay">
                    <h1>Pastry Wheel</h1>
                    <h5>Just be sure to make level slices rather than one long cut, as dragging a knife through pastry can tear and toughen it.</h5>
                    <Button
                        id='6235cb74e8bdf693524b50d3'
                        buttonStyle='btn--outline'
                        buttonSize='btn--medium'
                    >
                        SHOP NOW
                    </Button>
                </div>
            </Carousel.Item>            
            <Carousel.Item className="carouselFit" interval={1500}>
                <img
                    className="d-block w-100 carouselImg"
                    src={require('../../assets/images/imgCarousel3.jpg')}
                    alt="First slide"
                />
                <div className="overlay">
                    <h1>Ramekins</h1>
                    <h5>They come in handy for serving desserts to a crowd.</h5>
                      <Button
                        id='6235c94be8bdf693524b5093'
                        buttonStyle='btn--outline'
                        buttonSize='btn--medium'
                    >
                        SHOP NOW
                    </Button>
                </div>
            </Carousel.Item>
        </Carousel>
    )
}