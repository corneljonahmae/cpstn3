import Button from '../Button/Button';
import { Row, Col } from 'react-bootstrap';


export default function CarouselSection() {
    return (
        <div className="my-5">
            <Row>
                <Col xs={12} md={7} className="p-0">
                    <img
                        className="img-fluid"
                        src={require('../../assets/images/imgFeatured1.png')}
                        alt="First slide"
                    />
                </Col>
                <Col xs={12} md={5}>
                    <div className="d-flex flex-column align-items-center justify-content-center p-3 mt-xl-3">
                        <h4>Measuring Cups</h4>
                        <p className="w-lg-75 gray">
                        Baking is all about precision, so having a full set of measuring cups and spoons on hand is a must.
                        </p>
                        <Button
                        id='6235cb86e8bdf693524b50d9'
                        buttonStyle='btn--secondary'
                        buttonSize='btn--medium'>
                        Learn More
                        </Button>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col xs={12} md={7} className="p-0 order-lg-2">
                    <img
                        className="img-fluid"  
                        src={require('../../assets/images/imgFeatured2.png')}
                        alt="First slide"
                    />
                </Col>
                <Col xs={12} md={5}>
                    <div className="d-flex flex-column align-items-center justify-content-center p-3 mt-xl-3 order-lg-1">
                        <h4>Rubber Spatula</h4>
                        <p className="w-lg-75 gray">
                        Silicone scrapers will stand up to high heat better than rubber ones.
                        </p>
                        <Button
                        id='622d28d1d9f7689bbd71047c'
                        buttonStyle='btn--secondary'
                        buttonSize='btn--medium'>
                        Learn More
                        </Button>
                    </div>
                </Col>
            </Row>            
            <Row>
                <Col xs={12} md={7} className="p-0">
                    <img
                        className="img-fluid"
                        src={require('../../assets/images/imgFeatured3.png')}
                        alt="First slide"
                    />
                </Col>
                <Col xs={12} md={5}>
                    <div className="d-flex flex-column align-items-center justify-content-center p-3 mt-xl-3">
                        <h4></h4>
                        <p className="w-lg-75 gray">
                           This tool can be used to grease a pan before pouring in cake batter, to coat the dough with melted butter or egg wash, or to "paint" milk on top of a pie crust.
                        </p>
                        <Button
                        id='622e5f4d058c97fbfc04566f'
                        buttonStyle='btn--secondary'
                        buttonSize='btn--medium'>
                        Learn More
                        </Button>
                    </div>
                </Col>
            </Row>
        </div>
    )
}